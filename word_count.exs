filename = IO.gets("File to get word count from (h for help):") |> String.trim()

if filename == "h" do
  IO.puts("""
  Usage: [filename] -[flags]

  Flags
  -l    display linecount
  -c    display character count
  -w    display word count (default)

  Multiple flags may be used. Example for character and word count:

  somefile.txt -cw
  """)
else
  parts = String.split(filename, "-")
  filename = List.first(parts) |> String.trim()

  flags =
    case(Enum.at(parts, 1)) do
      # default to word count
      nil -> ["w"]
      chars -> String.split(chars, "") |> Enum.filter(fn x -> x != "" end)
    end

  content = File.read!(filename)

  words =
    content
    |> String.split(~r{(\\n|[^\w'])+})
    |> Enum.filter(fn word -> word != "" end)

  lines =
    content
    |> String.split("\n")
    |> Enum.filter(fn line -> line != "" end)

  chars =
    content
    |> String.split("")
    |> Enum.filter(fn x -> x != "" end)

  word_count =
    words
    |> Enum.count()

  num_lines =
    lines
    |> Enum.count()

  char_count =
    chars
    |> Enum.count()

  Enum.each(flags, fn flag ->
    case flag do
      "l" -> IO.puts("Line count: #{num_lines}")
      "c" -> IO.puts("Character count: #{char_count}")
      "w" -> IO.puts("Word count: #{word_count}")
    end
  end)
end
